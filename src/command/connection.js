var motd = require("./server-query").motd;

module.exports = {

    pass : function (server, user, password) {
        if (user.isRegistered()) {
            throw ['ERR_ALREADYREGISTRED'];
        } else if (password === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "PASS"];
        }

        // MUST precede the latter of the NICK/USER combination
        if (user.nick === "*" && user.username === "*") {
            user.password = password;
        }
    },

    nick : function (server, user, nickname) {
        if (nickname === undefined) {
            throw ['ERR_NONICKNAMEGIVEN'];
        } else if (nickname.length < 3 || !nickname.charAt(0).match(/[A-Za-z]/i)) {
            throw ['ERR_ERRONEUSNICKNAME', nickname];
        }

        // Check if nick is available
        if (server.isNickAvailable(nickname, user.password)) {
            var oldName = user.name;
            var oldNick = user.nick;

            // Change user nickname
            user.nick = nickname;
            // Update user name every time that nick or username changes
            user.updateName();

            // Return response if user is registered
            if (user.isRegistered()) {
                user.broadcast(oldName, ["NICK", ":" + nickname]);

                // Update authenticated nickname
                var authenticated = server.getAuthenticated(oldNick);
                if (authenticated !== null) {
                    authenticated.nick = nickname;
                }

                // Notify other users of channel
                user.channels.forEach(function (c) {
                    c.users.forEach(function (u) {
                        // Send only to other users
                        if (u !== user) {
                            u.broadcast(oldName, ["NICK", ":" + nickname]);
                        }
                    })
                })
            }
            // Register user if he gave a username
            else if (user.username !== "*") {
                this.register(server, user)
            }
        }
    },

    user : function (server, user, username, mode, unused, realName) {
        if (user.isRegistered()) {
            throw ['ERR_ALREADYREGISTRED'];
        } else if (username === undefined || mode === undefined || unused === undefined || realName === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "USER"];
        }

        // Change username, realName and mode
        // TODO understand better user modes
        user.username = username;
        user.realName = realName;
        user.mode = '+i';
        // Update user name every time that nick or username changes
        user.updateName();

        // Register user if he gave a nickname
        if (user.nick !== "*") {
            this.register(server, user);
        }
    },

    register : function (server, user) {
        // Check if user has a nickname and username
        if (user.nick !== "*" && user.username !== "*") {
            user.registered = true;
            user.broadcast(server.name, ["001", user.nick, ":Welcome to the Group 2 Internet Relay Network " + user.name]);
            user.broadcast(server.name, ["002", user.nick, ":Your host is " + server.name + ", running version " + server.version]);
            user.broadcast(server.name, ["003", user.nick, ":This server was created on " + server.createdAt]);
            user.broadcast(server.name, ["004", user.nick, server.name, server.version, "DOQRSZaghilopswz CFILMPQSbcefgijklmnopqrstvz bkloveqjfI"]);
            motd(server, user, undefined);
            user.broadcast(user.nick, ["MODE", user.nick, ":" + user.mode]);

            // Create or update authenticated user
            if (user.password !== "") {
                server.addAuthenticated(user.nick, user.username, user.password);
            }
        }
    },

    oper : function (user, username, password) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (username === undefined || password === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "OPER"];
        }

        // No operators registered on this server
        throw 'ERR_NOOPERHOST';
    },

    mode : function (user, nick, flag) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (nick === undefined || flag === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "MODE"];
        } else if (user.nick !== nick) {
            throw ['ERR_USERSDONTMATCH'];
        } else if (!user.isModeValid(flag)) {
            throw ['ERR_UMODEUNKNOWNFLAG'];
        }
        // TODO understand better user modes
    },

    service : function () {
        // Not implemented
    },

    quit : function (server, user, message) {
        if (user.destroyed) {
            return;
        }

        // Default quit message
        if (message === undefined) {
            message = "Client Quit";
        }

        // Show quit message and close connection
        user.destroyed = true;
        user.broadcast(user.name, ["QUIT", ":" + message]);
        server.quitUser(user, message);
        user.socket.end();
        user.socket.destroy();
        console.log("User " + user.name + " has left the server");
    },

    squit : function (server, user, target, comment) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target === undefined || comment === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "SQUIT"];
        } else if (target !== undefined && target !== server.name) {
            throw ['ERR_NOSUCHSERVER', target];
        }

        // No operators registered on this server
        throw ['ERR_NOPRIVILEGES'];
    }
};