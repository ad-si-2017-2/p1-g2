module.exports = {

    kill : function (server, user, nickname, comment) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (nickname === undefined || nickname.length === 0) {
            throw ['ERR_NEEDMOREPARAMS', "KILL"];
        }

        // No operators registered on this server
        throw ['ERR_NOPRIVILEGES'];
    },

    ping : function (server, user, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "PING"];
        }

        // Return pong message to userpo
        user.broadcast(server.name, ["PONG", server.name, target]);
    },

    pong : function (server, user, target) {
        // No reply message
    },

    error : function (server, user, message) {
        // No reply message (ERROR command is for use by servers)
    }
};