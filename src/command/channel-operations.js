module.exports = {

    join: function (server, user, channel, key) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (channel === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "JOIN"];
        }

        if (channel === "0") {
            user.partAllChannels();
        } else {
            var channels = channel.split(",");
            var keys = key !== undefined ? key.split(",") : [];

            // Join all channels sent by user
            for (var i = 0; i < channels.length; i++) {
                var c = channels[i].toLowerCase().trim();

                // Validate channel name
                if (c.charAt(0) !== '#') {
                    user.error(['ERR_NOSUCHCHANNEL', c]);
                }
                else {
                    server.joinChannel(server, user, c, keys[i]);
                }
            }
        }
    },

    part: function (server, user, channel, message) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (channel === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "PART"];
        }

        var channels = channel.split(",");

        // Part all channels sent by user
        for (var i = 0; i < channels.length; i++) {
            var channelName = channels[i].toLowerCase().trim();
            var c = server.getChannel(channelName);

            // Check if channel exists
            if (c === undefined) {
                user.error(['ERR_NOSUCHCHANNEL', c]);
            }
            // Part channel
            else {
                c.partUser(user, message);
            }
        }
    },

    mode: function (server, user, channel, mode) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (channel === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "MODE"];
        }

        var c = server.getChannel(channel);

        // Check if channel exists
        if (c === undefined) {
            throw ['ERR_NOSUCHCHANNEL', channel];
        }
        // Check if user is on the channel
        else if (mode !== undefined && user.isOnChannel()) {
            throw ['ERR_NOTONCHANNEL', channel];
        }
    },

    names: function (server, user, channel) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        }

        if (channel === undefined) {
            // List of all channels and their users
            server.channels.forEach(function (c) {
                c.namesReply(user);
            });
        } else {
            var channels = channel.split(",");

            // Get all channels sent by user
            for (var i = 0; i < channels.length; i++) {
                var channelName = channels[i].toLowerCase().trim();
                var c = server.getChannel(channelName);

                // Check if channel exists
                if (c !== undefined) {
                    // Send names reply
                    c.namesReply(user);
                } else {
                    user.error(['ERR_NOSUCHSERVER', channelName]);
                }
            }
        }
        user.broadcast(user.server.name, [366, user.nick, "*", ":End of /NAMES list"]);
    },

    topic: function (server, user, channel, topic) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (channel === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "TOPIC"];
        }

        var c = server.getChannel(channel);

        // Check if channel exists
        if (c === undefined) {
            throw ['ERR_NOSUCHCHANNEL', channel];
        }
        // Check if user is on the channel
        else if (user.isOnChannel()) {
            throw ['ERR_NOTONCHANNEL', channel];
        }

        // Check the topic for the channel
        if (topic === undefined) {
            c.topicReply(user);
        } else {
            c.setTopic(user, topic);
        }
    },

    list: function (server, user, channel) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        }

        if (channel === undefined) {
            // List of all channels and their topics
            server.channels.forEach(function (c) {
                c.listReply(user);
            });
        } else {
            var channels = channel.split(",");

            // Get all channels sent by user
            for (var i = 0; i < channels.length; i++) {
                var channelName = channels[i].toLowerCase().trim();
                var c = server.getChannel(channelName);

                // Check if channel exists
                if (c !== undefined) {
                    // Send list reply
                    c.listReply(user);
                } else {
                    user.error(['ERR_NOSUCHCHANNEL', channelName]);
                }
            }
        }
        user.broadcast(user.server.name, [323, user.nick, ":End of LIST"]);
    },

    invite: function (server, user, nickname, channel) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (nickname === undefined || channel === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "INVITE"];
        }

        var c = server.getChannel(channel);
        var u = server.getUser(nickname);

        // Check if channel exists
        if (c === undefined) {
            throw ['ERR_NOSUCHCHANNEL', channel];
        }
        // Check if user exists
        else if (u === undefined) {
            throw ['ERR_NOSUCHNICK', nickname];
        }
        // Check if sender is on the channel
        else if (!user.isOnChannel(channel)) {
            throw ['ERR_NOTONCHANNEL', channel];
        }

        c.invite(user, u);
    },

    kick: function (server, user, channel, nickname, comment) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (channel === undefined || nickname === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "KICK"];
        }

        var channels = channel.split(",");
        var nicknames = nickname.split(",");

        /**
         *  For the message to be syntactically correct, there MUST be
         *  either one channel parameter and multiple user parameter, or as many
         *  channel parameters as there are user parameters
         */
        if (channels.length !== 1 && channels.length !== nicknames.length) {
            throw ['ERR_NEEDMOREPARAMS', "KICK"];
        } else {
            // Get all channels sent by user
            for (var i = 0; i < channels.length; i++) {
                var channelName = channels[i].toLowerCase().trim();
                var c = server.getChannel(channelName);

                // Check if channel exists
                if (c === undefined) {
                    user.error(['ERR_NOSUCHCHANNEL', channelName]);
                }

                // One channel parameter and multiple user parameter
                if (channels.length === 1) {
                    // Get all users sent by user
                    for (var j = 0; j < nicknames.length; j++) {
                        c.kickUser(user, nicknames[j], comment);
                    }
                }
                // Many channel parameters as there are user parameters
                else {
                    c.kickUser(user, nicknames[i], comment);
                }
            }
        }
    }
};