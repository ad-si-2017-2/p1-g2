module.exports = {

    motd : function (server, user, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target !== undefined && target !== server.name) {
            throw ['ERR_NOSUCHSERVER', target];
        }

        // Show server message of the day
        user.broadcast(server.name, ["375", user.nick, ":- " + server.name + " Message of the day - "]);
        for (var i = 0; i < server.motd.length; i++) {
            user.broadcast(server.name, ["372", user.nick, ":- " + server.motd[i]]);
        }
        user.broadcast(server.name, ["376", user.nick, ":End of MOTD command"]);
    },

    lusers : function (server, user, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target !== undefined && target !== server.name) {
            throw ['ERR_NOSUCHSERVER', target];
        }

        // Show lusers message
        user.broadcast(server.name, ["251", user.nick, ":There are " + server.users.length + " users and " + 0 + " services on " + 1 + " servers"]);
        user.broadcast(server.name, ["252", user.nick, 0, ":IRC operators online"]);
        user.broadcast(server.name, ["253", user.nick, 0, ":unknown connection(s)"]);
        user.broadcast(server.name, ["254", user.nick, server.channels.length, ":channels formed"]);
        user.broadcast(server.name, ["255", user.nick, ":I have " + server.users.length + " clients and " + 1 + " servers"]);
    },

    version : function (server, user, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target !== undefined && target !== server.name) {
            throw ['ERR_NOSUCHSERVER', target];
        }

        // Show version message
        user.broadcast(server.name, ["351", user.nick, server.version, server.name, ":UFG, Aplicacoes Distribuidas (Projeto 1 - Grupo 2)"]);
    },

    stats : function (server, user, query, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (query === undefined) {
            throw ['ERR_NEEDMOREPARAMS', "STATS"];
        } else if (target !== undefined && target !== server.name) {
            throw ['ERR_NOSUCHSERVER', target];
        }

        // Returns a list of the server's connections
        if (query === 'l') {
            user.broadcast(server.name, ["211", user.nick, user.nick + "[~" + user.name + "]", "0 75 6 11 0 :107 0 -"]);
        }
        // Returns the usage count for each of commands supported by the server
        else if (query === 'm') {
            for (var k in server.commandsCount) {
                if (server.commandsCount.hasOwnProperty(k) && server.commandsBytes.hasOwnProperty(k)) {
                    user.broadcast(server.name, ["212", user.nick, k, server.commandsCount[k], server.commandsBytes[k], ":0"]);
                }
            }
        }
        // Returns a list of configured privileged users
        else if (query === 'o') {
            throw ['ERR_NOPRIVILEGES'];
        }
        // Returns a string showing how long the server has been up.
        else if (query === 'u') {
            var timeDiff = Math.abs(new Date().getTime() - server.createdAt.getTime());
            var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
            var diffSeconds = Math.ceil(timeDiff / 1000);
            var d = new Date(null);
            d.setSeconds(diffSeconds);
            user.broadcast(server.name, ["242", user.nick, ":Server Up " + diffDays + " day(s), " + d.toISOString().substr(11, 8)]);
        }

        // Show end of stats report
        user.broadcast(server.name, ["219", user.nick, query, ":End of /STATS report"]);
    },

    links : function (server, user, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target !== undefined && target !== server.name) {
            throw ['ERR_NOSUCHSERVER', target];
        }

        // Show links message
        user.broadcast(server.name, ["364", user.nick, server.name, server.name, ":0 UFG Brasil"]);
        user.broadcast(server.name, ["365", user.nick, "*", ":End of LINKS list"]);
    },

    time : function (server, user, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target !== undefined && target !== server.name) {
            throw ['ERR_NOSUCHSERVER', target];
        }

        // Show time message
        user.broadcast(server.name, ["391", user.nick, server.name, ":" + new Date()]);
    },

    connect : function (server, user) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        }

        // No operators registered on this server
        throw ['ERR_NOPRIVILEGES'];
    },

    trace : function (server, user, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        }

        if (target !== undefined && target !== server.name) {
            var u = server.getUser(target);

            if (u === undefined) {
                throw ['ERR_NOSUCHSERVER', target];
            }

            // / Show trace user
            user.broadcast(server.name, ["205", user.nick, "User", "users", u.nick + "[" + u.hostname + "]"]);
            user.broadcast(server.name, ["262", user.nick, target, ":End of TRACE"]);
        } else {
            // Show trace server
            user.broadcast(server.name, ["205", user.nick, "User", "users", user.nick + "[" + user.hostname + "]"]);
            user.broadcast(server.name, ["262", user.nick, server.name, ":End of TRACE"]);
        }
    },

    admin : function (server, user, target) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target !== undefined && target !== server.name) {
            throw ['ERR_NOSUCHSERVER', target];
        }

        // Show admin message
        user.broadcast(server.name, ["256", user.nick, server.name, ":Administrative info"]);
        user.broadcast(server.name, ["257", user.nick, ":Aplicacoes Distribuidas - Sistemas da Informacao, UFG"]);
        user.broadcast(server.name, ["258", user.nick, ":https://gitlab.com/ad-si-2017-2/p1-g2"]);
        user.broadcast(server.name, ["259", user.nick, ":marceloakira@inf.ufg.br"]);
    },

    info : function (server, user) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        }

        // Show info message
        user.broadcast(server.name, ["371", user.nick, ":IRC --"]);
        user.broadcast(server.name, ["371", user.nick, ":Based on the original code written by Jarkko Oikarinen"]);
        user.broadcast(server.name, ["371", user.nick, ":Copyright 2017 University of Goias, Brasil"]);
        user.broadcast(server.name, ["371", user.nick, ":"]);
        user.broadcast(server.name, ["371", user.nick, ":This program is free software; you can redistribute it and/or"]);
        user.broadcast(server.name, ["371", user.nick, ":modify it under the terms of the GNU General Public License as"]);
        user.broadcast(server.name, ["371", user.nick, ":published by the Free Software Foundation; either version 2, or"]);
        user.broadcast(server.name, ["371", user.nick, ":(at your option) any later version."]);
        user.broadcast(server.name, ["371", user.nick, ":"]);
        user.broadcast(server.name, ["371", user.nick, ":Alex Alves Maia - @alexmaia.ufg"]);
        user.broadcast(server.name, ["371", user.nick, ":Bruno Braz Silveira - @brazsilveira"]);
        user.broadcast(server.name, ["371", user.nick, ":Fernando Junio Cunha e Sousa - @fjcs7"]);
        user.broadcast(server.name, ["371", user.nick, ":Matheus Galhardo - @matheusgalhardo"]);
        user.broadcast(server.name, ["371", user.nick, ":"]);
        user.broadcast(server.name, ["371", user.nick, ":"]);
        user.broadcast(server.name, ["371", user.nick, ":Visit the gitlab repository at: https://gitlab.com/ad-si-2017-2/p1-g2"]);
        user.broadcast(server.name, ["374", user.nick, ":End of /INFO list"]);
    }
};