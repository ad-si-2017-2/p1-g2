module.exports = {

    privmsg : function (server, user, target, message) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (target === undefined || target.length === 0) {
            throw ['ERR_NORECIPIENT'];
        } else if (message === undefined || message.length === 0) {
            throw ['ERR_NOTEXTTOSEND'];
        }

        // Privmsg for channels
        if (target.charAt(0) === '#') {
            var c = server.getChannel(target);
            if (c !== undefined) {
                c.privmsg(user, message);
            } else {
                throw ['ERR_NOSUCHNICK', target];
            }
        }
        // Privmsg for users
        else {
            var u = server.getUser(target);
            if (u !== undefined) {
                u.broadcast(user.name, ["PRIVMSG", target, ":" + message]);
            } else {
                throw ['ERR_NOSUCHNICK', target];
            }
        }
    },

    notice : function (server, user, target, message) {
        /**
         *  MUST NOT send any error reply back to the client on
         *  receipt of a notice
         */
        if (!user.isRegistered()) {
            return;
        } else if (target === undefined || target.length === 0) {
            return;
        } else if (message === undefined || message.length === 0) {
            return;
        }

        // Notice for channels
        if (target.charAt(0) === '#') {
            var c = server.getChannel(target);
            if (c !== undefined) {
                c.notice(user, message);
            }
        }
        // Notice for users
        else {
            var u = server.getUser(target);
            if (u !== undefined) {
                u.broadcast(user.name, ["NOTICE", target, ":" + message]);
            }
        }
    }
};