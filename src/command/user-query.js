module.exports = {

    who : function (server, user, mask) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (mask === undefined || mask.length === 0) {
            throw ['ERR_NEEDMOREPARAMS', "WHO"];
        }

        // Who for channels
        if (mask.charAt(0) === '#') {
            var c = server.getChannel(mask);
            if (c !== undefined) {
                c.users.forEach(function (u) {
                    // Check if user is operator
                    var chan = c.isOperator(u) ? 'H@' : 'H';
                    // Send who reply
                    user.broadcast(server.name, ["352", user.nick, c.name, u.nick, u.hostname, server.name, u.nick, chan, ":0", u.realName]);
                });
            }
        }
        // Who for users
        else {
            var u = server.getUser(mask);
            if (u !== undefined) {
                // Send who reply
                user.broadcast(server.name, ["352", user.nick, '*', mask, u.hostname, server.name, u.nick, 'H', ":0", u.realName]);
            }
        }

        // Send who end of list
        user.broadcast(server.name, ["315", user.nick, mask, ":End of /WHO list"]);
    },

    whois : function (server, user, mask) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (mask === undefined || mask.length === 0) {
            throw ['ERR_NEEDMOREPARAMS', "WHOIS"];
        }


        var u = server.getUser(mask);
        if (u !== undefined) {
            // Send whois reply
            user.broadcast(server.name, ["311", user.nick, mask, "~" + u.realName, u.hostname, "*" , ":" + u.username]);
            user.broadcast(server.name, ["312", user.nick, mask, server.name, ":UFG, Brasil"]);
        } else {
            throw ['ERR_NOSUCHNICK', mask]
        }

        // Send whois end of list
        user.broadcast(server.name, ["318", user.nick, mask, ":End of /WHOIS list"]);
    },

    whowas : function (server, user, nickname) {
        if (!user.isRegistered()) {
            throw ['ERR_NOTREGISTERED'];
        } else if (nickname === undefined || nickname.length === 0) {
            throw ['ERR_NONICKNAMEGIVEN'];
        }

        // Send whowas message (not saving user history)
        user.broadcast(server.name, ["406", user.nick, nickname, ":There was no such nickname"]);
        user.broadcast(server.name, ["369", user.nick, nickname, ":End of WHOWAS"]);
    }
};