function Channel(server, name, key, type, topic) {
    this.server = server;
    this.name = name;
    this.users = [];
    this.key = key;
    this.capacity = 5;
    this.operators = {};
    // '=' public channels, 'p' private channels, '@' secret channels
    this.type = type;
    this.topic = topic;
}

Channel.prototype.joinUser = function (user) {
    // Check if user isn't already on the channel
    var index = this.users.indexOf(user);

    if (index <= -1) {
        // Add new user to channel
        this.users.push(user);
        user.channels.push(this);

        // Notify all users
        var channelName = this.name;
        this.users.forEach(function (u) {
            u.broadcast(user.name, ["JOIN", channelName]);
        });

        // Send topic reply if exists
        if (this.hasTopic()) {
            this.topicReply(user);
        }
        // Send name reply to new user
        this.namesReply(user);
        // Send end of names reply
        this.endOfNamesReply(user);
    }
};

Channel.prototype.partUser = function (user, message) {
    // Find user index
    var index = this.users.indexOf(user);

    if (index > -1) {
        // Remove user from channel
        this.users.splice(index, 1);
        // Remove operator privileges
        this.removeOperator(user);

        // Remove channel from user
        var channelIndex = user.channels.indexOf(this);
        if (channelIndex > -1) {
           user.channels.splice(channelIndex, 1);
        }

        // Sennd part message
        user.broadcast(user.name, ['PART', this.name]);

        // Notify other users (user was already removed)
        var params = (message === undefined || message.length === 0) ? ["PART", this.name] : ["PART", this.name, ":" + message];
        this.users.forEach(function (u) {
            u.broadcast(user.name, params);
        });
    } else {
        user.error(['ERR_NOTONCHANNEL', this.name]);
    }
};

Channel.prototype.kickUser = function (operator, nickname, comment) {
    // Check if operator is on the channel
    if (!operator.isOnChannel(this.name)) {
        return operator.error(['ERR_NOTONCHANNEL', this.name]);
    }
    // Check if operator is really operator of the channel
    if (!this.isOperator(operator)) {
        return operator.error(['ERR_CHANOPRIVSNEEDED', this.name]);
    }

    var user = operator.server.getUser(nickname);
    // Check if user exists
    if (user === undefined) {
        return operator.error(['ERR_NOSUCHNICK', nickname]);
    }
    // Check if user is on the channel
    if (!user.isOnChannel(this.name)) {
        return operator.error(['ERR_USERNOTINCHANNEL', nickname + " " + this.name]);
    }

    // Remove user from channel
    this.users.splice(this.users.indexOf(user), 1);
    // Remove operator privileges
    this.removeOperator(user);

    // Remove channel from user
    var channelIndex = user.channels.indexOf(this);
    if (channelIndex > -1) {
        user.channels.splice(channelIndex, 1);
    }

    // Sennd kick message
    var params = ['KICK', this.name, nickname, ":" + (comment !== undefined ? comment : nickname)];
    operator.broadcast(operator.name, params);
    user.broadcast(operator.name, params);
};

Channel.prototype.quitUser = function (user, message) {
    // Find user index
    var index = this.users.indexOf(user);

    if (index > -1) {
        // Remove user from channel
        this.users.splice(index, 1);
        // Remove operator privileges
        this.removeOperator(user);

        // Notify other users (user was already removed)
        this.users.forEach(function (u) {
            u.broadcast(user.name, ["QUIT", ":" + message]);
        });
    }
};

Channel.prototype.addOperator = function (user) {
    // Add user as operator of this channel
    this.operators[user.name] =  user;
};

Channel.prototype.removeOperator = function (user) {
    // Remove operator privelege of user on this channel
    if (this.isOperator(user)) {
        delete this.operators[user.name];
    }
};

Channel.prototype.isOperator = function (user) {
    // Check if user is an operator of this channel
    return user.name in this.operators;
};

Channel.prototype.invite = function (user, newUser) {
    // Check if user is already on the channel
    if (newUser.isOnChannel(this.name)) {
        throw ['ERR_USERONCHANNEL', newUser.nick + " " + this.name];
    }

    // Send invitation message to new user
    newUser.broadcast(user.name, ['INVITE', newUser.nick, this.name]);
    /**
     * 341 RPL_INVITING <nick> <channel>
     * Note that RFC1459 documents the parameters in the reverse order.
     * The format given here is the format used on production servers,
     * and should be considered the standard reply above that given by RFC1459.
     */
    // Send confirmation to sender
    user.broadcast(user.server.name, [341, user.nick, newUser.nick, this.name]);
};

Channel.prototype.privmsg = function (user, message) {
    if (user.isOnChannel(this.name)) {
        var name = this.name;
        this.users.forEach(function (u) {
            if (u !== user) {
                u.broadcast(user.name, ["PRIVMSG", name, ":" + message]);
            }
        });
    } else {
        throw ['ERR_CANNOTSENDTOCHAN', this.name];
    }
};

Channel.prototype.notice = function (user, message) {
    if (user.isOnChannel(this.name)) {
        var name = this.name;
        this.users.forEach(function (u) {
            if (u !== user) {
                u.broadcast(user.name, ["NOTICE", name, ":" + message]);
            }
        });
    }
};

Channel.prototype.setTopic = function (user, topic) {
    if (!this.isOperator(user)) {
        throw ['ERR_CHANOPRIVSNEEDED', this.name]
    }

    // Set new topic
    this.topic = topic;

    // Notify all users
    var channelName = this.name;
    this.users.forEach(function (u) {
        u.broadcast(user.name, ['TOPIC', channelName, ":" + topic]);
    });
};

Channel.prototype.hasTopic = function () {
    return this.topic.length > 0;
};

Channel.prototype.topicReply = function (user) {
    if (this.hasTopic()) {
        user.broadcast(user.server.name, [332, user.nick, this.name, ":" + this.topic]);
    } else {
        user.broadcast(user.server.name, [331, user.nick, this.name, ":No topic is set"]);
    }
};

Channel.prototype.listReply = function (user) {
    user.broadcast(user.server.name, [322, user.nick, this.name, this.users.length, ":" + this.topic]);
};

Channel.prototype.namesReply = function (user) {
    var chan = this;
    var userNames = ":";
    this.users.forEach(function (u) {
        userNames += chan.isOperator(u) ? "@" : "";
        userNames += u.nick + " ";
    });
    user.broadcast(user.server.name, [353, user.nick, this.type, this.name, userNames.trim()]);
};

Channel.prototype.endOfNamesReply = function (user) {
    user.broadcast(user.server.name, [366, user.nick, this.name, ":End of /NAMES list"]);
};

module.exports = Channel;