function Err(name, value) {
    this.name = name;
    this.value = value;
}

Err.prototype.getValue = function() {
    return this.value;
};

Err.prototype.getCode = function() {
    return ERRORS[this.name].code;
};

Err.prototype.getDescription = function() {
    return ERRORS[this.name].description;
};

const ERRORS = {
    ERR_NOSUCHNICK :        {code: 401, description: "No such nick/channel"},
    ERR_NOSUCHSERVER :      {code: 402, description: "No such server"},
    ERR_NOSUCHCHANNEL :     {code: 403, description: "No such channel"},
    ERR_CANNOTSENDTOCHAN :  {code: 404, description: "Cannot send to channel"},
    ERR_NORECIPIENT :       {code: 411, description: "No recipient given"},
    ERR_NOTEXTTOSEND :      {code: 412, description: "No text to send"},
    ERR_UNKNOWNCOMMAND :    {code: 421, description: "Unknown command"},
    ERR_NONICKNAMEGIVEN :   {code: 431, description: "No nickname given"},
    ERR_ERRONEUSNICKNAME :  {code: 432, description: "Erroneous Nickname"},
    ERR_NICKNAMEINUSE :     {code: 433, description: "Nickname is already in use"},
    ERR_USERNOTINCHANNEL :  {code: 441, description: "They aren't on that channel"},
    ERR_NOTONCHANNEL :      {code: 442, description: "You're not on that channel"},
    ERR_USERONCHANNEL :     {code: 443, description: "is already on channel"},
    ERR_NOTREGISTERED :     {code: 451, description: "You have not registered"},
    ERR_NEEDMOREPARAMS :    {code: 461, description: "Not enough parameters"},
    ERR_ALREADYREGISTRED :  {code: 462, description: "Unauthorized command (already registered)"},
    ERR_CHANNELISFULL :     {code: 471, description: "Cannot join channel (+l)"},
    ERR_BADCHANNELKEY :     {code: 475, description: "Cannot join channel (+k)"},
    ERR_NOPRIVILEGES :      {code: 481, description: "Permission Denied- You're not an IRC operator"},
    ERR_CHANOPRIVSNEEDED :  {code: 482, description: "You're not channel operator"},
    ERR_NOOPERHOST :        {code: 491, description: "No O-lines for your host"},
    ERR_UMODEUNKNOWNFLAG :  {code: 501, description: "Unknown MODE flag"},
    ERR_USERSDONTMATCH :    {code: 502, description: "Cannot change mode for other users"}
};

module.exports = Err;