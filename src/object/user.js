var Err = require('./err');

function User(server, socket) {
    this.name = "";
    this.nick = "*";
    this.username = "*";
    this.realName = "";
    this.password = "";
    this.channels = [];
    this.socket = socket;
    this.server = server;
    this.commandData = "";
    this.mode = "";
    this.hostname = "localhost";
    this.registered = false;
    this.destroyed = false;
}

// Update user name (it's necessary every time that nick or username changes)
User.prototype.updateName = function() {
    this.name = this.nick + "!" + this.username + "@" + this.hostname;
};

// Check if connection registration is completed
User.prototype.isRegistered = function() {
    return this.registered;
};

// Check if connection registration is completed
User.prototype.isModeValid = function(flag) {
    return ['a', 'i', 'w', 'r', 'o', 'O', 's', '+a', '+i', '+w', '+r', '+o', '+O', '+s', '-a', '-i', '-w', '-r', '-o', '-O', '-s'].indexOf(flag) > -1;
};

// Check if connection registration is completed
User.prototype.isOnChannel = function(name) {
    for (var i = 0; i < this.channels.length; i++) {
        if (this.channels[i].name === name) {
            return true;
        }
    }
    return false;
};

// Leave all channel
User.prototype.partAllChannels = function() {
    var user = this;

    // Part all channels
    while (this.channels.length > 0) {
        user.channels[0].partUser(user, undefined);
    }
};

// Send a broadcast to user
 User.prototype.broadcast = function(name, params) {
     if (this.socket.destroyed) {
         return;
     }

     var message = ":" + name;
     params.forEach(function (p) {
         message += " " + p;
     });
     message += "\r\n";
     this.socket.write(message);
 };

 // Send a error message to user
User.prototype.error = function(error) {
    var err = new Err(error[0], error[1]);
    var message = ":" + this.server.name + " " + err.getCode() + " " + this.nick;
    if (err.getValue() !== undefined) {
        message += " " + err.getValue();
    }
    message += " :" + err.getDescription() + "\r\n";
    this.socket.write(message);
};

 module.exports = User;

/** @namespace socket.remotePort */
/** @namespace socket.remoteAddress */