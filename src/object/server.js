var Channel = require('./channel');

function Server() {
    this.name = 'localhost';
    this.port = 6667;
    this.ip = '127.0.0.1';
    this.version = 'adsi-p1-g2-0.0.1';
    this.createdAt = new Date();
    this.motd = [
        '',
        '=================================================================',
        ' Ola!',
        '',
        ' Seja bem-vindo ao Servidor IRC utilizando sockets em nodejs do',
        ' Grupo 2 da materia Aplicacoes Distribuidas do Curso de Sistemas',
        ' de Informacao da UFG.',
        '',
        ' Esta materia e ministrada pelo Professor Me. @marceloakira',
        '',
        '=================================================================',
        ' Os membros participantes deste Grupo sao:',
        '',
        '  Fernando Junio Cunha e Sousa   @fjcs7',
        '  Bruno Braz Silveira            @bbrazsilveira',
        '  Alex Alves                     @alexmaia.ufg',
        '  Matheus Galhardo               @matheusgalhardo',
        '',
        '=================================================================',
        '',
        ' Acesse https://gitlab.com/ad-si-2017-2/p1-g2 para mais informacoes',
        ''
    ];
    this.users = [];
    this.channels = [];

    // Save nick, username and passwords from users authenticated
    this.authenticated = [];

    this.commandsCount = {};
    this.commandsBytes = {};
}

function Authenticate(nick, username, password) {
    this.nick = nick;
    this.username = username;
    this.password = password;
}

Server.prototype.joinUser = function (user) {
    this.users.push(user);
};

Server.prototype.quitUser = function (user, message) {
    this.users.splice(this.users.indexOf(user), 1);

    // Remove user from all channels
    user.channels.forEach(function (c) {
        c.quitUser(user, message);
    })
};

Server.prototype.isNickAvailable = function (nick, password) {
    // Check if nick is in use
    this.users.forEach(function (u) {
        if (u.nick.toUpperCase() === nick.toUpperCase()) {
            throw ['ERR_NICKNAMEINUSE', nick];
        }
    });
    // Check nick authentication
    this.authenticated.forEach(function (a) {
        if (a.nick.toUpperCase() === nick.toUpperCase() && a.password !== password) {
            throw ['ERR_NICKNAMEINUSE', nick];
        }
    });
    return true;
};

Server.prototype.getAuthenticated = function (nick) {
    this.authenticated.forEach(function (a) {
        if (a.nick.toUpperCase() === nick.toUpperCase()) {
            return a;
        }
    });
    return null;
};

Server.prototype.addAuthenticated = function (nick, username, password) {
    var authenticated = this.getAuthenticated(nick);

    // Update authenticated user
    if (authenticated !== null) {
        authenticated.username = username;
    }
    // Create authenticated user
    else {
        this.authenticated.push(new Authenticate(nick, username, password));
    }
    return null;
};

Server.prototype.joinChannel = function (server, user, name, key) {
    // Find channel by name
    var channel = this.getChannel(name);

    // Create a secret channel if it doesn't exist
    if (channel === undefined) {
        channel = new Channel(this, name, key, '@', "");
        this.channels.push(channel);

        // Add operator privilege to user manage the new channel
        channel.addOperator(user);
    } else {
        // Authenticate channel password
        if (channel.key !== key) {
            return user.error(['ERR_BADCHANNELKEY', channel.name]);
        }
        // Check channel max capacity
        else if (channel.users.length >= channel.capacity) {
            return user.error(['ERR_CHANNELISFULL', channel.name]);
        }
    }

    // Add user to channel
    channel.joinUser(user);
};

Server.prototype.getChannel = function (name) {
    // Find channel by name
    name = name.trim().toLowerCase();
    for (var i = 0; i < this.channels.length; i++) {
        var c =  this.channels[i];
        if (c.name === name) {
            return c;
        }
    }

    return undefined;
};

Server.prototype.getUser = function (nick) {
    // Find user by name
    for (var i = 0; i < this.users.length; i++) {
        var u =  this.users[i];
        if (u.nick === nick) {
            return u;
        }
    }

    return undefined;
};

module.exports = Server;