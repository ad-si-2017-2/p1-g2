// Import library and class
net = require('net');
dns = require('dns');
connection = require('./command/connection');
messaging = require('./command/messaging');
channelOperations = require('./command/channel-operations');
messaging = require('./command/messaging');
serverQuery = require('./command/server-query');
userQuery = require('./command/user-query');
miscellaneous = require('./command/miscellaneous');
User = require('./object/user');
Server = require('./object/server');
Channel = require('./object/channel');


// Create a new server
var server = new Server();
// Create public channels
server.channels.push(new Channel(server, "#public", undefined, '=', ""));
server.channels.push(new Channel(server, "#ufg", undefined, '=', "Bem vindo calouros 2017/2!"));

// Create tcp server on port 6667
net.createServer(function(socket) {

    // Create a new user
    var user = new User(server, socket);
    // Add user to server
    server.joinUser(user);

    // Notice connection logs
    console.log("User " + user.name + " has joined the server");
    user.broadcast(server.name, ["NOTICE", "*", ":*** Looking up your hostname..."]);

    // Reverse dns lookup
    if (net.isIP(socket.remoteAddress)) {
        dns.reverse(socket.remoteAddress, function (err, domains) {
            if (err !== null || domains.length === 0) {
                user.broadcast(server.name, ["NOTICE", "*", ":*** Could not find your hostname"]);
            } else {
                user.hostname = domains[0];
                user.broadcast(server.name, ["NOTICE", "*", ":*** Found your hostname"]);
            }
        });
    }

    // Handle incoming messages from clients.
    socket.on('data', function(data) {
        onDataReceived(String(data));
    });

    // Remove the client from the list when it leaves
    socket.on('end', function() {
        onCloseConnection();
    });

    function onDataReceived(data) {
        // Return if user connection is destroyed
        if (user.destroyed) {
            return;
        }

        // Append data until user send break line
        user.commandData += data;

        // Check if user has sent break line
        if (data.indexOf("\n") !== -1) {
            var commandData = user.commandData.trim();
            user.commandData = "";

            // Separate commands from a single string
            var commandsData = commandData.split("\n");
            for (var i = 0; i < commandsData.length; i++) {
                var command = formatData(commandsData[i].trim());

                // Execute command sent by user
                if (command.length > 0) {
                    onCommandReceived(command);
                    console.log("User " + user.name + " sent \"" + command + "\"");
                }
            }
        }
    }

    function formatData(data) {
        var d = "";
        // Format data removing \b backspace caractere
        for (var i = 0; i < data.length; i++) {
            if (data[i] === '\b') {
                if (d.length > 0) {
                    d = d.substr(0, d.length-1);
                }
            } else {
                d += data[i];
            }
        }
        return d;
    }

    function onCloseConnection() {
        connection.quit(server, user, undefined);
    }

    function onCommandReceived(commandData) {
        var params;

        // Split command data and check if contains any text defined by ' :'
        if (commandData.indexOf(" :") !== -1) {
            var paramsWithText = commandData.split(" :");
            params = paramsWithText[0].split(" ");
            params.push(paramsWithText[1]);
        } else {
            params = commandData.split(" ");
        }

        // Get command from parameters
        var command = params[0].toUpperCase();

        try {
            // Check command sent and delegate it
            // Connection Registration
            if (command === "PASS") {
                connection.pass(server, user, params[1]);
            } else if (command === "NICK") {
                connection.nick(server, user, params[1]);
            } else if (command === "USER") {
                connection.user(server, user, params[1], params[2], params[3], params[4]);
            } else if (command === "OPER") {
                connection.oper(user, params[1], params[2]);
            } else if (command === "MODE" && params[1] !== undefined && params[1].charAt(0) !== '#') {
                connection.mode(user, params[1], params[2]);
            } else if (command === "QUIT") {
                connection.quit(server, user, params[1]);
            } else if (command === "SQUIT") {
                connection.squit(server, user, params[1], params[2]);
            }
            // Channel operations
            else if (command === "JOIN") {
                channelOperations.join(server, user, params[1], params[2]);
            } else if (command === "PART") {
                channelOperations.part(server, user, params[1], params[2]);
            } else if (command === "MODE") {
                channelOperations.mode(server, user, params[1], params[2]);
            } else if (command === "TOPIC") {
                channelOperations.topic(server, user, params[1], params[2]);
            } else if (command === "NAMES") {
                channelOperations.names(server, user, params[1]);
            } else if (command === "LIST") {
                channelOperations.list(server, user, params[1]);
            } else if (command === "INVITE") {
                channelOperations.invite(server, user, params[1], params[2]);
            } else if (command === "KICK") {
                channelOperations.kick(server, user, params[1], params[2], params[3]);
            }
            // Sending messages
            else if (command === "PRIVMSG") {
                messaging.privmsg(server, user, params[1], params[2]);
            }else if (command === "NOTICE") {
                messaging.notice(server, user, params[1], params[2]);
            }
            // Server queries and commands
            else if (command === "MOTD") {
                serverQuery.motd(server, user, params[1]);
            } else if (command === "LUSERS") {
                serverQuery.lusers(server, user, params[1]);
            } else if (command === "VERSION") {
                serverQuery.version(server, user, params[1]);
            } else if (command === "STATS") {
                serverQuery.stats(server, user, params[1], params[2]);
            } else if (command === "LINKS") {
                serverQuery.links(server, user, params[1]);
            } else if (command === "TIME") {
                serverQuery.time(server, user, params[1]);
            } else if (command === "CONNECT") {
                serverQuery.connect(server, user);
            } else if (command === "TRACE") {
                serverQuery.trace(server, user, params[1]);
            } else if (command === "ADMIN") {
                serverQuery.admin(server, user, params[1]);
            } else if (command === "INFO") {
                serverQuery.info(server, user);
            }
            // User based queries
            else if (command === "WHO") {
                userQuery.who(server, user, params[1]);
            } else if (command === "WHOIS") {
                userQuery.whois(server, user, params[1]);
            } else if (command === "WHOWAS") {
                userQuery.whowas(server, user, params[1]);
            }
            // Miscellaneous message
            else if (command === "KILL") {
                miscellaneous.kill(server, user, params[1], params[2]);
            } else if (command === "PING") {
                miscellaneous.ping(server, user, (commandData.indexOf(" :") !== -1) ? ":" + params[1] : params[1]);
            } else if (command === "PONG") {
                miscellaneous.pong(server, user, params[1]);
            } else if (command === "ERROR") {
                miscellaneous.error(server, user, params[1]);
            }
            // Unknown command
            else {
                return user.error(['ERR_UNKNOWNCOMMAND', params[0]]);
            }

            // Increment comand count
            server.commandsCount[command] = (server.commandsCount[command] === undefined) ? 1 : server.commandsCount[command] + 1;
            // Increment comand bytes
            server.commandsBytes[command] = (server.commandsBytes[command] === undefined) ? (commandData.length * 2) : server.commandsBytes[command] + (commandData.length * 2);
        } catch (err) {
            // Treat errors
            if (Array.isArray(err)) {
                user.error(err);
            } else {
                console.log(err);
            }
        }
    }
}).listen(server.port);

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 6667");