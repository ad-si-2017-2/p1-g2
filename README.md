# Introdução
Olá !

Seja bem-vindo ao **Projeto 1 - Servidor IRC utilizando sockets em nodejs** do **Grupo 2** da matéria **Aplicações Distribuídas** do Curso de Sistemas de Informação da [UFG](https://www.ufg.br/)!

Disciplina ministrada pelo [**Professor Me. Marcelo Akira**](@marceloakira)!

Os membros participantes deste Grupo são:

- [Alex Alves Maia](@alexmaia.ufg) *- Documentação*

- [Bruno Braz Silveira](@bbrazsilveira) *- Desenvolvedor*

- [Fernando Junio Cunha e Sousa](@fjcs7) *- Líder/Desenvolvedor*

- [Matheus Galhardo](@matheusgalhardo) *- Documentação*

# Quer saber mais sobre nosso projeto?
**Acesse a documentação completa na [Wiki](https://gitlab.com/ad-si-2017-2/p1-g2/wikis/home) do nosso projeto.**